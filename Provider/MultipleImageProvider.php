<?php

namespace Megacoders\MediaBundle\Provider;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;
use Imagine\Image\ImagineInterface;
use Megacoders\MediaBundle\Entity\Media;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\CDN\CDNInterface;
use Sonata\MediaBundle\Generator\GeneratorInterface;
use Sonata\MediaBundle\Metadata\MetadataBuilderInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\ImageProvider;
use Sonata\MediaBundle\Thumbnail\ThumbnailInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\File;

class MultipleImageProvider extends ImageProvider
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /** @var MediaInterface[] */
    private $newMedias = [];

    /**
     * @param string                   $name
     * @param EntityManagerInterface   $entityManager
     * @param Filesystem               $filesystem
     * @param CDNInterface             $cdn
     * @param GeneratorInterface       $pathGenerator
     * @param ThumbnailInterface       $thumbnail
     * @param array                    $allowedExtensions
     * @param array                    $allowedMimeTypes
     * @param ImagineInterface         $adapter
     * @param MetadataBuilderInterface $metadata
     */
    public function __construct($name, EntityManagerInterface $entityManager, Filesystem $filesystem, CDNInterface $cdn, GeneratorInterface $pathGenerator, ThumbnailInterface $thumbnail, array $allowedExtensions = array(), array $allowedMimeTypes = array(), ImagineInterface $adapter, MetadataBuilderInterface $metadata = null)
    {
        parent::__construct($name, $filesystem, $cdn, $pathGenerator, $thumbnail, $allowedExtensions, $allowedMimeTypes, $adapter, $metadata);

        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildCreateForm(FormMapper $formMapper)
    {
        $formMapper->add('binaryContent', FileType::class, ['multiple' => true]);
    }

    /**
     * @throws \RuntimeException
     * @param \Sonata\MediaBundle\Model\MediaInterface $media
     */
    protected function fixBinaryContent(MediaInterface $media)
    {
        if ($media->getBinaryContent() === null || $media->getBinaryContent() instanceof File) {
            return;
        }

        $binaryContents = $media->getBinaryContent();

        if (is_array($binaryContents)) {
            $media->setBinaryContent(array_shift($binaryContents));

            $this->newMedias = [];

            foreach ($binaryContents as $binaryContent) {
                $multiMedia = clone $media;
                $multiMedia->setBinaryContent($binaryContent);

                $this->newMedias[] = $multiMedia;
            }

        } else {
            parent::fixBinaryContent($media);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist(MediaInterface $media)
    {
        parent::postPersist($media);

        if ($this->newMedias) {
            foreach ($this->newMedias as $multiMedia) {
                $this->entityManager->persist($multiMedia);
            }

            $this->entityManager->flush($this->newMedias);
            $this->newMedias = [];
        }
    }
}
