<?php

namespace Megacoders\MediaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\MediaBundle\Provider\MediaProviderInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="media__gallery")
 * @Gedmo\TranslationEntity(class="Translations\GalleryTranslation")
 */
class Gallery extends BaseGallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=64)
     * @var string
     */
    protected $context;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $defaultFormat = MediaProviderInterface::FORMAT_REFERENCE;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $enabled;

    /**
     * @ORM\Column(type="datetime")
     * @var \Datetime
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     * @var \Datetime
     */
    protected $createdAt;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Megacoders\MediaBundle\Entity\Translations\GalleryTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
