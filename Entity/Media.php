<?php

namespace Megacoders\MediaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="media__media")
 * @Gedmo\TranslationEntity(class="Translations\MediaTranslation")
 */
class Media extends BaseMedia
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $name;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $enabled = false;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $providerName = "image";

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $providerStatus;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $providerReference;

    /**
     * @ORM\Column(type="json", nullable = true)
     * @var array
     */
    protected $providerMetadata = array();

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    protected $width;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    protected $height;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     * @var float
     */
    protected $length;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    protected $contentType;

    /**
     * @ORM\Column(name="content_size", type="integer", nullable=true)
     * @var int
     */
    protected $size;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $copyright;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $authorName;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @var string
     */
    protected $context;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var bool
     */
    protected $cdnIsFlushable = false;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @var string
     */
    protected $cdnFlushIdentifier;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $cdnFlushAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    protected $cdnStatus;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Megacoders\MediaBundle\Entity\Translations\MediaTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

}
