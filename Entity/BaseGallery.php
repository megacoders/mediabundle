<?php

namespace Megacoders\MediaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Megacoders\MediaBundle\Model\Gallery;

abstract class BaseGallery extends Gallery
{
    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->galleryHasMedias = new ArrayCollection();
    }

    /**
     * Pre Persist method.
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Pre Update method.
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
