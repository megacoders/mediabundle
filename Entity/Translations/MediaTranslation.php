<?php

namespace Megacoders\MediaBundle\Entity\Translations;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\MediaBundle\Entity\Media;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="media__media_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_media__media_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class MediaTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\MediaBundle\Entity\Media", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Media
     */
    protected $object;

}
