<?php

namespace Megacoders\MediaBundle\Entity\Translations;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\MediaBundle\Entity\Gallery;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="media__gallery_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_media__gallery_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class GalleryTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\MediaBundle\Entity\Gallery", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Gallery
     */
    protected $object;

}
