- install *sonata-project/easy-extends-bundle* (if not yet)
- include in *AppKernel.php*
```
new JMS\SerializerBundle\JMSSerializerBundle(),
new Sonata\IntlBundle\SonataIntlBundle(),
new Sonata\MediaBundle\SonataMediaBundle(),
new Megacoders\MediaBundle\MegacodersMediaBundle(),
```
- include in *routing.yml*
```
app_media:
    resource: "@MegacodersMediaBundle/Resources/config/routing.yml"
```
- include in *config.yml*
```
doctrine:
    dbal:
        ... 
        types:
            json:     Sonata\Doctrine\Types\JsonType
    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true
        mappings:
            MegacodersMediaBundle: ~
            SonataMediaBundle: ~            
```    
```
sonata_media:
    class:
        media: Megacoders\MediaBundle\Entity\Media
        gallery: Megacoders\MediaBundle\Entity\Gallery
        gallery_has_media: Megacoders\MediaBundle\Entity\GalleryHasMedia
        category: Megacoders\ClassificationBundle\Entity\Category
    db_driver: doctrine_orm # or doctrine_mongodb, doctrine_phpcr it is mandatory to choose one here
    default_context: default # you need to set a context
    contexts:
        default:  # the default context is mandatory
            providers:
                - sonata.media.provider.youtube
                - sonata.media.provider.image
                - sonata.media.provider.file

            formats:
                small: { width: 100 , quality: 70}
                big:   { width: 500 , quality: 70}

    cdn:
        server:
            path: /uploads/media # http://media.sonata-project.org/

    filesystem:
        local:
            directory:  "%kernel.root_dir%/../web/uploads/media"
            create:     false

    providers:
        image:
            resizer: sonata.media.resizer.square
```
```
sonata_classification:
    class:
        ... 
        media:        Megacoders\MediaBundle\Entity\Media
        ...
```                                
- generate database structure
- create uploads dir (if not yet) 
```
    $ mkdir web/uploads
    $ mkdir web/uploads/media
    $ chmod -R 0777 web/uploads
```    
- fix *sonata-project/media-bundle* from *sonata-patch.tar.gz* archive            