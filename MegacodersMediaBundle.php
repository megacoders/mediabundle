<?php

namespace Megacoders\MediaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MegacodersMediaBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataMediaBundle';
    }

}
